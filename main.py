#! /usr/bin/python
# -*- coding: utf-8 -*-

## This is the compiler for cantabscript
## Cantabscript compiles down to python, and is then executed

from docx import Document
import re #regular expressions

document = Document('source.docx')
source = []
for para in document.paragraphs:
    new_string = ""
    prev = False
    for char in para.text:
        if ord(char) > 60000:
            if prev == False:
                new_string = new_string + "<windings1>"
                prev = True
            new_string = new_string + chr(ord(char) - 61505 + ord('a'))
        else:
            if prev == True:
                new_string = new_string + "</windings1>"
                prev = False
            
            # fix for Windows so that the print function works as there is unicode weirdness
            if ord(char) == 8217:
                new_string = new_string + "'"
            else:
                new_string = new_string + char
    if prev == True:
        new_string = new_string + "</windings1>"    
    source.append(new_string)
                
            


python_code = ""
indent = 0

for line in source:
    print line
    print python_code

    # this is the start of any generic method
    if len(line) > 26 and line[:12] == "THIS IS THE " and line[12:23] != "END OF THE " and line[-26:] == "<windings1>bit</windings1>":
        function_name = line[12:]
        function_name = function_name[:-27]
        if function_name == "MAIN":
            function_name = "main"
        python_code = python_code + "def " + function_name + "():\n"
        indent = indent + 1

    # this is the end of any generic method
    if len(line) > 26 and line[:12] == "THIS IS THE " and line[12:23] == "END OF THE " and line[-26:] == "<windings1>bit</windings1>":
        python_code = python_code + "\n"
        indent = indent - 1

    # this gets a method to run (one without parameters)
    if len(line) > 16 and line[:8] == "GET THE " and line[-6:] == "TO RUN":
        function_name = line[8:]
        function_name = function_name[:-7]
        for i in range(indent):
            python_code = python_code + "    "
        python_code = python_code + function_name + "()\n"

    if len(line) > 10 and line[:10] == "WRITE OUT ":
        for i in range(indent):
            python_code = python_code + "    "
        hashtag = line.find('#')
        pound = line.find(u'£')
        python_code = python_code + 'print "' + line[hashtag + 1:pound] + '"\n'
        

    if len(line) >= 40 and line[:9] == "COUNT TO " and line[-30:] == " AND DO STUFF AT THE SAME TIME":
        counter = line[9:-30]
        for i in range(indent):
            python_code = python_code + "    "
        python_code = python_code + "for i in range(" + counter + "):\n"
        indent = indent + 1

    if line == "STOP COUNTING":
        indent = indent - 1

    if len(line) >= 44:
        # Could be an if statement
        split_line = line.split()
        sec_1 = " ".join(split_line[:3]) #split_line[0] + " " + split_line[1] + " " + split_line[2]
        sec_2 = " ".join(split_line[4:7]) # split_line[4] + " " + split_line[5] + " " + split_line[6]
        sec_3 = " ".join(split_line[-6:]) # split_line[-6] + " " + split_line[-5] + " " + split_line[-4] + " " + split_line[-3] + " " + split_line[-2] + " " + split_line[-1]
        sec_4 = " ".join(split_line[-2:]) # split_line[-2] + " " + split_line[-1]
        if sec_1 == u"I'M INDECISIVE IS":
            # If statement
            for i in range(indent):
                python_code = python_code + "    "
            indent = indent + 1
            python_code = python_code + "if (" + split_line[3][1:] # First character is a '%'
            if sec_2 == "THE SAME AS":
                python_code = python_code + " == " 
            elif sec_2 == "NOT THE SAME":
                python_code = python_code + " != "
            else:
                print sec_2
                print "AAAAAAAAAAAAAARRRRRRRRRRRRRRRGGGGGGGGGGGGGGGGHHHHHHHHHHHHHHHHHHH1"
                exit()
            if (sec_3 == "NOT A WORD OF A LIE?"):
                python_code = python_code + "True):\n"
            elif (sec_4 == "FAKE NEWS?"):
                python_code = python_code + "False):\n"
            else:
                print sec_3
                print sec_4
                print "AAAAAAAAAAAAAARRRRRRRRRRRRRRRGGGGGGGGGGGGGGGGHHHHHHHHHHHHHHHHHHH2"
                exit()
        
    if line == "DECISION MADE" or line == "DEFINITELY DID STUFF": # EndIf
        indent = indent - 1

    if len(line) >= 38:
        ## Could be a variable decleration
        split_line = line.split()
        sec_1 = " ".join(split_line[:5])
        sec_2 = " ".join(split_line[6:8])
        sec_3 = " ".join(split_line[8:])
        print sec_1
        print sec_2
        print sec_3
        if sec_1 == "I WANNA HAVE A VARIABLE" and sec_2 == "WITH CONTENTS":
            ## Defo a variable assignment
            for i in range(indent):
                python_code = python_code + "    "
            python_code = python_code + split_line[5][1:] # First character is '%'
            python_code = python_code + " = "
            if sec_3 == "FAKE NEWS":
                python_code = python_code + "False\n"
            elif sec_3 == "NOT A WORD OF A LIE":
                python_code = python_code + "True\n"
            elif sec_3[0] == '#' and sec_3[-1] == u'£':
                python_code = python_code + '"' + sec_3[1:-1] + '"\n'
            else:
                python_code = python_code + sec_3 + "\n"

    if line == "DECISION MADE BUT IF NOT STILL DO STUFF":
        for i in range(indent - 1): # The -1 is becuase the else wants to be 1 block back from wherever we were
                python_code = python_code + "    "
        python_code = python_code + "else:\n"
                
            

python_code = python_code + "main()\n"
            
print python_code

exec(python_code)
